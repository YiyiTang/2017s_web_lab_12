package ictgradschool.web.lab12.ex3;


import org.jooq.*;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import ictgradschool.web.lab12.ex3.generated.Tables;


class ActInfo {
    public String film;
    public String role;

    public ActInfo(String film, String role) {
        this.film = film;
        this.role = role;
    }
}

class FilmInfo {
    public String person;
    public String role;

    public FilmInfo(String person, String role) {
        this.person = person;
        this.role = role;
    }
}

class FilmInGenre {
    public String film;

    public FilmInGenre(String film) {
        this.film = film;
    }
}

public class Excercise03 {

    public void start() {

        Properties dbProps = new Properties();

        try(FileInputStream fIn = new FileInputStream("postgres.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try(Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            DSLContext create = DSL.using(conn, SQLDialect.POSTGRES);
            List<ActInfo> infos = findFilmsByActor(create, "Keanu", "Reeves");
            for (ActInfo info : infos) {
                System.out.println(info.film + "(" + info.role + ")");
            }
            System.out.println("\r\nCheck film info for Angry Birds:");
            List<FilmInfo>  filmInfos = findPeopleInFilm(create, "Angry Birds");

            for (FilmInfo filmInfo : filmInfos) {
                System.out.println(filmInfo.person + "(" + filmInfo.role + ")");
            }

            System.out.println("\r\nCheck films for Horror:");
            List<FilmInGenre>  fs = filmByGenre(create, "Horror");

            for (FilmInGenre f : fs) {
                System.out.println(f.film);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) {
        Excercise03 ex3 = new Excercise03();
        ex3.start();
    }

    private List<ActInfo> findFilmsByActor(DSLContext create, String fname, String lname) {

        List<ActInfo> infos = new ArrayList<>();

        SelectConditionStep<Record> result = create.select().from(Tables.PFILMS_ACTOR)
                .join(Tables.PFILMS_PARTICIPATES_IN).onKey()
                .join(Tables.PFILMS_ROLE).onKey()
                .join(Tables.PFILMS_FILM).onKey()
                .where(Tables.PFILMS_ACTOR.ACTOR_FNAME.eq(fname).and(Tables.PFILMS_ACTOR.ACTOR_LNAME.eq(lname)));

        for (Record r : result) {

            String name = r.getValue(Tables.PFILMS_FILM.FILM_TITLE);
            String role = r.getValue(Tables.PFILMS_ROLE.ROLE_NAME);
            ActInfo info = new ActInfo(name, role);
            infos.add(info);
        }

        return infos;
    }

    private List<FilmInfo> findPeopleInFilm(DSLContext create, String film) {

        List<FilmInfo> infos = new ArrayList<>();

        SelectConditionStep<Record> result = create.select().from(Tables.PFILMS_ACTOR)
                .join(Tables.PFILMS_PARTICIPATES_IN).onKey()
                .join(Tables.PFILMS_ROLE).onKey()
                .join(Tables.PFILMS_FILM).onKey()
                .where(Tables.PFILMS_FILM.FILM_TITLE.eq(film));

        for (Record r : result) {

            String name = r.getValue(Tables.PFILMS_ACTOR.ACTOR_FNAME) + " "
                    + r.getValue(Tables.PFILMS_ACTOR.ACTOR_LNAME);
            String role = r.getValue(Tables.PFILMS_ROLE.ROLE_NAME);
            FilmInfo info = new FilmInfo(name, role);
            infos.add(info);
        }

        return infos;
    }

    private List<FilmInGenre> filmByGenre(DSLContext create, String genre) {

        List<FilmInGenre> infos = new ArrayList<>();

        SelectConditionStep<Record2<String, String>> result = create.select(
                Tables.PFILMS_FILM.GENRE_NAME,
                Tables.PFILMS_FILM.FILM_TITLE).from(Tables.PFILMS_FILM)
                .where(Tables.PFILMS_FILM.GENRE_NAME.eq(genre));

        for (Record r : result) {

            String name = r.getValue(Tables.PFILMS_FILM.FILM_TITLE);
            infos.add(new FilmInGenre(name));
        }

        return infos;
    }

}
