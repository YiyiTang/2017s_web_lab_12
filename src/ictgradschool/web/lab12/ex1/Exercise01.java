package ictgradschool.web.lab12.ex1;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;

public class Exercise01 {

    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try(FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");

            while (true) {
                String partial = getUserInput("Please input a title:");

                if (partial != null && !partial.isEmpty()) {
                    try (PreparedStatement statement  = conn.prepareStatement(
                            "SELECT * FROM simpledao_articles WHERE title LIKE ?; ")){

                        statement.setString(1,  "%" + partial + "%");

                        try (ResultSet r = statement.executeQuery()){
                            final int COL_TITLE = 2;
                            final int COL_BODY = 3;
                            while (r.next()) {
                                System.out.println("=======" + r.getString(COL_TITLE) + "=======");
                                System.out.println(r.getString(COL_BODY));
                            }
                        }

                    }
                } else {
                    System.out.println("Title cannnot be empty!");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }



    }

    private static String getUserInput(String title){

        try {
            System.out.println(title);
            String input = in.nextLine();
            //System.out.println(input);
            return input;
        } catch (NoSuchElementException e) {
            return null; // End of file
        } catch (IllegalStateException e) {
            System.err.println("An error has occurred in the Keyboard.readInput() method.");
            e.printStackTrace();
            System.exit(-1);
        }
        return null;
    }

}
