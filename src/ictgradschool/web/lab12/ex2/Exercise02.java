package ictgradschool.web.lab12.ex2;

import ictgradschool.web.lab12.ex2.Article;
import ictgradschool.web.lab12.ex2.ArticleDAO;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;

public class Exercise02 {

    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try(FileInputStream fIn = new FileInputStream("postgres.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");

            while (true) {
                String partial = getUserInput("Please input a title:");

                if (partial != null && !partial.isEmpty()) {

                    List<Article> list = ArticleDAO.getArticleLike(conn, partial);
                    for (Article article : list) {
                        System.out.println("========== " + article.getTitle() + " ==========");
                        System.out.println(article.getBody());
                    }

                } else {
                    System.out.println("Title cannnot be empty!");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }



    }

    private static String getUserInput(String title){

        try {
            System.out.println(title);
            String input = in.nextLine();
            //System.out.println(input);
            return input;
        } catch (NoSuchElementException e) {
            return null; // End of file
        } catch (IllegalStateException e) {
            System.err.println("An error has occurred in the Keyboard.readInput() method.");
            e.printStackTrace();
            System.exit(-1);
        }
        return null;
    }

}
