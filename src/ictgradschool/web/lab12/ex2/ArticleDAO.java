package ictgradschool.web.lab12.ex2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ArticleDAO {

    public static List<Article> getArticleLike(Connection conn, String pattern) {

        List<Article> list = new ArrayList<>();

        try (PreparedStatement statement  = conn.prepareStatement(
                "SELECT * FROM simpledao_articles WHERE title LIKE ?; ")){

            statement.setString(1,  "%" + pattern + "%");

            try (ResultSet r = statement.executeQuery()){
                final int COL_TITLE = 2;
                final int COL_BODY = 3;
                final int COL_ID = 1;
                while (r.next()) {

                    list.add(new Article(r.getInt(COL_ID), r.getString(COL_TITLE), r.getString(COL_BODY)));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

}
